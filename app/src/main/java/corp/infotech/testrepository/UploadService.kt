package corp.infotech.testrepository

import com.google.gson.Gson
import com.squareup.moshi.Moshi
import corp.infotech.testrepository.NetworkExecutor.RequestExecutor
import corp.infotech.testrepository.NetworkManager.INetworkManager
import corp.infotech.testrepository.NetworkManager.NetworkManager
import corp.infotech.testrepository.NetworkParser.GsonParser
import corp.infotech.testrepository.NetworkParser.MoshiParser
import corp.infotech.testrepository.NetwrokCore.*
import okhttp3.OkHttpClient
import java.io.File

/**
 * Created by nikitafeshchun on 15.08.17.
 */

interface IUploadService
{
    fun uploadFile(file: File, success: (result: FileResponse?) -> Unit, failure: (error: Exception) -> Unit)
    fun uploadFile(file: File, success: () -> Unit, failure: (error: Exception) -> Unit)
}

class UploadService: IUploadService {

    val manager: INetworkManager = NetworkManager(
            NetworkCore(
                    RequestExecutor(
                            baseUrl = "http://mapissues-master.infotech.team/modules/oek/sg/photo/content?token=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDI5NzMwMDcsInVzZXJJZCI6IjEzMyIsInJlZnJlc2hUb2tlbiI6ImV5SmhiR2NpT2lKSVV6STFOaUo5LmV5SmxlSEFpT2pBc0luVnpaWEpKWkNJNklqRXpNeUo5LnhTczdCTkxNMFJjSFBpUHl5c0o5LTltZUpjOGZBcXZpQlRoVVluSHZzN1kifQ.4_lOIx3PSYbeBC89HnzD3ur8XZDU0br0DGz02jh2poE",
                            client = OkHttpClient()
                    )
            ),
            GsonParser(Gson()))

    override fun uploadFile(file: File, success: (result: FileResponse?) -> Unit, failure: (error: Exception) -> Unit)
    {
        val config = RequestConfig(
                route = "",
                type = RequestType.POST,
                contentType = ContentType.MULTIPART,
                parseType = FileResponse::class.java
        )

        manager.asyncRequest<FileResponse>(
                config = config,
                file = file,
                success = success,
                failure = failure)

    }

    override fun uploadFile(file: File, success: () -> Unit, failure: (error: Exception) -> Unit)
    {
        val config = RequestConfig(
                route = "",
                type = RequestType.POST,
                contentType = ContentType.MULTIPART,
                parseType = FileResponse::class.java
        )

        manager.asyncRequest(
                config = config,
                file = file,
                success = success,
                failure = failure)

    }
}