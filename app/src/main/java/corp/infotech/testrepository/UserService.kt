package corp.infotech.testrepository

import com.google.gson.Gson
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import corp.infotech.testrepository.NetworkExecutor.RequestExecutor
import corp.infotech.testrepository.NetworkManager.INetworkManager
import corp.infotech.testrepository.NetworkManager.NetworkManager
import corp.infotech.testrepository.NetworkParser.GsonParser
import corp.infotech.testrepository.NetworkParser.MoshiParser
import corp.infotech.testrepository.NetwrokCore.*
import okhttp3.OkHttpClient
import java.io.File

/**
 * Created by nikitafeshchun on 15.08.17.
 */

interface IUserService
{
    fun user(success: (result: DummyObject?) -> Unit, failure: (error: Exception) -> Unit)
    fun users(success: (result: List<DummyObject>?) -> Unit, failure: (error: Exception) -> Unit)
    fun users() : Pair<List<DummyObject>?,Exception?>?

    fun addUser(user: DummyObject, success: (Boolean) -> Unit)
    fun addUsers(users: List<DummyObject>, success: (Boolean) -> Unit)
    fun removeUser(user: DummyObject, success: (Boolean) -> Unit)
    fun removeUsers(users: List<DummyObject>, success: (Boolean) -> Unit)
}

class UserService: IUserService {

    val manager: INetworkManager = NetworkManager(
            NetworkCore(
                    RequestExecutor(
                            baseUrl = "http://5.references-dev.infotech.team/api/core/ds/data/object",
                            client = OkHttpClient()
                    )
            ),
            GsonParser(Gson()))

    override fun user(success: (result: DummyObject?) -> Unit, failure: (error: Exception) -> Unit) {
        users(success = { success(it?.firstOrNull()) }, failure = failure)
    }

    override fun users(success: (result: List<DummyObject>?) -> Unit, failure: (error: Exception) -> Unit)
    {
        val config = RequestConfig(
                route = "/ds1",
                type = RequestType.GET,
                parseType = Types.newParameterizedType(List::class.java,DummyObject::class.java),
                contentType = ContentType.JSON)

        manager.asyncRequest<List<DummyObject>>(
                config = config,
                data = "",
                success = success,
                failure = failure)
    }

    override fun users(): Pair<List<DummyObject>?,Exception?>? {

        val config = RequestConfig(
                route = "/ds1",
                type = RequestType.GET,
                parseType = Types.newParameterizedType(List::class.java,DummyObject::class.java),
                contentType = ContentType.JSON)

        return manager.syncRequest(config, data = "")
    }

    override fun addUser(user: DummyObject, success: (Boolean) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addUsers(users: List<DummyObject>, success: (Boolean) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeUser(user: DummyObject, success: (Boolean) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeUsers(users: List<DummyObject>, success: (Boolean) -> Unit) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}