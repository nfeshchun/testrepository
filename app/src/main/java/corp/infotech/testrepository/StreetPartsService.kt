package corp.infotech.testrepository

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import corp.infotech.testrepository.NetworkExecutor.RequestExecutor
import corp.infotech.testrepository.NetworkManager.INetworkManager
import corp.infotech.testrepository.NetworkManager.NetworkManager
import corp.infotech.testrepository.NetworkParser.MoshiParser
import corp.infotech.testrepository.NetwrokCore.*
import okhttp3.OkHttpClient

/**
 * Created by nikitafeshchun on 15.08.17.
 */

class StreetPartsService
{
    val manager: INetworkManager = NetworkManager(
            NetworkCore(
                    RequestExecutor(
                            baseUrl = "http://mapissues-master.infotech.team/modules/oek/sg/street-part/31473/complete?token=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDI5NzA3MjYsInVzZXJJZCI6IjEzMyIsInJlZnJlc2hUb2tlbiI6ImV5SmhiR2NpT2lKSVV6STFOaUo5LmV5SmxlSEFpT2pBc0luVnpaWEpKWkNJNklqRXpNeUo5LnhTczdCTkxNMFJjSFBpUHl5c0o5LTltZUpjOGZBcXZpQlRoVVluSHZzN1kifQ.eEc03Qzlxs7Y0hCjJobvn3yU5jqoNVniIW_1ygqPiUM",
                            client = OkHttpClient()
                    )
            ),
            MoshiParser(Moshi.Builder().build()))

    fun completeStreet(success: () -> Unit, failure: (error: Exception) -> Unit)
    {
        val config = RequestConfig(
                route = "",
                type = RequestType.POST,
                contentType = ContentType.STRING)

        manager.asyncRequest(
                config = config,
                data = "",
                success = success,
                failure = failure)
    }
}