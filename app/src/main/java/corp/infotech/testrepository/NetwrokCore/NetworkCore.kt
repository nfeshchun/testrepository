package corp.infotech.testrepository.NetwrokCore

import corp.infotech.testrepository.NetworkExecutor.IRequestExecutor
import java.io.File

class NetworkCore(val executor: IRequestExecutor): INetworkCore {

    override fun syncRequest(config: RequestConfig, data: String): String? =
            executor.syncRequest(config, data)

    override fun asyncRequest(config: RequestConfig, file: File, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit): Unit =
            executor.asyncRequest(config, file, success, failure)

    override fun asyncRequest(config: RequestConfig, data: String, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit): Unit =
            executor.asyncRequest(config, data, success, failure)
}