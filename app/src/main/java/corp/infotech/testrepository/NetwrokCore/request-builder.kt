package corp.infotech.testrepository.NetwrokCore

import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody

private val HEADER_NAME = "Content-Type"

private fun constants(type: ContentType): String =
        when (type) {
            ContentType.JSON      -> "application/json"
            ContentType.STRING    -> "text/plain"
            ContentType.ENCODED   -> "application/x-www-form-urlencoded"
            ContentType.MULTIPART -> "multipart/form-data"
        }

fun Request.Builder.method(type: RequestType, requestBody: RequestBody): Request.Builder =
        when (type)
        {
            RequestType.GET    -> get()
            RequestType.POST   -> post(requestBody)
            RequestType.PUT    -> put(requestBody)
            RequestType.PATCH  -> patch(requestBody)
            RequestType.DELETE -> delete(requestBody)
        }

fun Request.Builder.contentType(contentType: ContentType): Request.Builder =
        addHeader(HEADER_NAME, constants(contentType))

fun Request.Builder.customHeaders(headers: Map<String, String>?): Request.Builder =
        apply { headers?.forEach { addHeader(it.key, it.value) } }

fun prepareContent(type: ContentType): MediaType =
        MediaType.parse(constants(type))!!