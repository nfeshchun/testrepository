package corp.infotech.testrepository.NetwrokCore

import java.io.File

interface INetworkCore {
    fun syncRequest(config: RequestConfig, data: String): String?
    fun asyncRequest(config: RequestConfig, file: File, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit)
    fun asyncRequest(config: RequestConfig, data: String, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit)
}