package corp.infotech.testrepository.NetwrokCore

import java.lang.reflect.Type

enum class ContentType
{
    JSON, STRING, ENCODED, MULTIPART
}

enum class RequestType
{
    GET,
    POST,
    PUT,
    PATCH,
    DELETE
}

data class RequestConfig(val route: String,
                         val type: RequestType,
                         val contentType: ContentType,
                         val parseType: Type? = null,
                         val headers: Map<String,String>? = null,
                         val parameters: Map<String,String>? = null)