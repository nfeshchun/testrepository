package corp.infotech.testrepository.NetworkManager

import corp.infotech.testrepository.NetwrokCore.RequestConfig
import java.io.File

/**
 * Created by nikitafeshchun on 15.08.17.
 */

interface INetworkManager {
    fun asyncRequest(config: RequestConfig, data: String, success: () -> Unit, failure: (error: Exception) -> Unit)
    fun asyncRequest(config: RequestConfig, file: File, success: () -> Unit, failure: (error: Exception) -> Unit)

    fun <T> asyncRequest(config: RequestConfig, data: String, success: (result: T?) -> Unit, failure: (error: Exception) -> Unit)
    fun <T> asyncRequest(config: RequestConfig, file: File, success: (result: T?) -> Unit, failure: (error: Exception) -> Unit)
    fun <T> syncRequest(config: RequestConfig, data: String): Pair<T?,Exception?>?
}