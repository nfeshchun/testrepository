package corp.infotech.testrepository.NetworkManager

import corp.infotech.testrepository.NetworkParser.INetworkParser
import corp.infotech.testrepository.NetwrokCore.INetworkCore
import corp.infotech.testrepository.NetwrokCore.RequestConfig
import java.io.File
import java.lang.reflect.Type

/**
 * Created by nikitafeshchun on 15.08.17.
 */

class NetworkManager(val networkCore: INetworkCore, val parser: INetworkParser): INetworkManager
{
    override fun <T> syncRequest(config: RequestConfig, data: String): Pair<T?, Exception?>? {
        val json = networkCore.syncRequest(config, data)
        return when (config.parseType) {
            null -> null
            else -> parse(config.parseType, json)
        }
    }

    override fun <T> asyncRequest(config: RequestConfig, data: String, success: (result: T?) -> Unit, failure: (error: Exception) -> Unit): Unit =
            networkCore.asyncRequest(config, data, success = { json ->
                when (config.parseType) {
                    null -> success(null)
                    else -> parse(config.parseType, json, success, failure)
                }
            }, failure = failure)

    override fun <T> asyncRequest(config: RequestConfig, file: File, success: (result: T?) -> Unit, failure: (error: Exception) -> Unit): Unit =
            networkCore.asyncRequest(config, file, success = { json ->
                when (config.parseType) {
                    null -> success(null)
                    else -> parse(config.parseType, json, success, failure)
                }
            }, failure = failure)

    override fun asyncRequest(config: RequestConfig, data: String, success: () -> Unit, failure: (error: Exception) -> Unit): Unit =
            asyncBlock(config, data, success, failure)

    override fun asyncRequest(config: RequestConfig, file: File, success: () -> Unit, failure: (error: Exception) -> Unit): Unit =
            asyncBlock(config, file, success, failure)



    private fun <T> asyncBlock(config: RequestConfig, data: T, success: () -> Unit, failure: (error: Exception) -> Unit): Unit =
            when (data) {
                is String -> networkCore.asyncRequest(config, data, success = { success() }, failure = failure)
                is File   -> networkCore.asyncRequest(config, data, success = { success() }, failure = failure)
                else      -> failure(Exception("Wrong Data"))
            }

    private fun <T> parse(type: Type, json: String?, success: (result: T?) -> Unit, failure: (error: Exception) -> Unit): Unit =
            try {
                val parse: T? = parser.parse(type, json)
                success(parse)
            }
            catch (error: Exception) {
                failure(error)
            }

    private fun <T> parse(type: Type, json: String?): Pair<T?, Exception?> =
            try {
                val parse: T? = parser.parse(type, json)
                Pair(parse, null)
            }
            catch (error: Exception) {
                Pair(null, error)
            }
}