package corp.infotech.testrepository.NetworkExecutor

import corp.infotech.testrepository.NetwrokCore.RequestConfig

interface IRequestExecutor {
    fun <T> syncRequest(config: RequestConfig, data: T): String?
    fun <T> asyncRequest(config: RequestConfig, data: T, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit)
}