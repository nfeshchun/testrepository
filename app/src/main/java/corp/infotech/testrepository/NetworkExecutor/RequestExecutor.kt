package corp.infotech.testrepository.NetworkExecutor

import corp.infotech.testrepository.NetwrokCore.*
import okhttp3.*
import okhttp3.internal.Util
import java.io.File
import java.io.IOException

class RequestExecutor(val baseUrl: String, val client: OkHttpClient): IRequestExecutor {

    private val FORM_NAME = "file"
    private val FAILED_MESSAGE = "Failed response"

    fun <T> buildBody(mediaType: MediaType, content: T) : RequestBody =
            when (content) {
                is String -> RequestBody.create(mediaType, content)
                is File ->
                    MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart(FORM_NAME, content.name, RequestBody.create(mediaType, content))
                            .build()
                else      -> Util.EMPTY_REQUEST
            }

    fun <T> buildRequest(config: RequestConfig, data: T): Request =
            Request.Builder()
                    .url(baseUrl + config.route)
                    .method(config.type, buildBody(prepareContent(config.contentType), data))
                    .customHeaders(config.headers)
                    .contentType(config.contentType)
                    .build()

    override fun <T> syncRequest(config: RequestConfig, data: T): String? =
            client.newCall(buildRequest(config, data))
                    .execute()
                    .body()
                    ?.string()

    override fun <T> asyncRequest(config: RequestConfig, data: T, success: (result: String?) -> Unit, failure: (error: Exception) -> Unit) {
        client.newCall(buildRequest(config, data)).enqueue(object : Callback {

            override fun onFailure(call: Call?, error: IOException): Unit =
                    failure(error)

            override fun onResponse(call: Call?, response: Response?): Unit =
                    when (response?.isSuccessful) {
                        true -> success(response.body()?.string())
                        else -> failure(Exception(FAILED_MESSAGE))
                    }
        })
    }
}