package corp.infotech.testrepository

import corp.infotech.testrepository.repository.RepositoryItem

/**
 * Created by nikitafeshchun on 15.08.17.
 */

data class DummyObject(
    val firstName: String,
    val lastName: String) : RepositoryItem

data class FileResponse(val uuid: String?, val data: String?, val success: Boolean?)