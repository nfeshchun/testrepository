package corp.infotech.testrepository.repository

import corp.infotech.testrepository.DummyObject
import corp.infotech.testrepository.IUserService

interface IRepository< T: RepositoryItem> {

    fun add(item: T, success: (Boolean) -> Unit)
    fun add(items: List<T>, success: (Boolean) -> Unit)

    fun remove(item: T, success: (Boolean) -> Unit)
    fun remove(items: List<T>, success: (Boolean) -> Unit)

    fun findAsync(success: (item: T?) -> Unit)
    fun findListAsync(success: (items: List<T>) -> Unit)
}

interface RepositoryItem

interface IUserRepository: IRepository<DummyObject>

class UserRepository(val userService: IUserService) : IUserRepository {
    
    override fun add(item: DummyObject, success: (Boolean) -> Unit) {
        userService.addUser(item, success)
    }

    override fun add(items: List<DummyObject>, success: (Boolean) -> Unit) {
        userService.addUsers(items, success)
    }

    override fun remove(item: DummyObject, success: (Boolean) -> Unit) {
        userService.removeUser(item, success = success)
    }

    override fun remove(items: List<DummyObject>, success: (Boolean) -> Unit) {
        userService.removeUsers(items, success)
    }

    override fun findAsync(success: (item: DummyObject?) -> Unit) {
        userService.user(success = success, failure = { success(null) })
    }

    override fun findListAsync(success: (items: List<DummyObject>) -> Unit) {
        userService.users(success = { success(it ?: emptyList()) }, failure = { success(emptyList()) })
    }
}