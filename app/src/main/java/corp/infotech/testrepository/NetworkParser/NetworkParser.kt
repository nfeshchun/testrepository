package corp.infotech.testrepository.NetworkParser

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by nikitafeshchun on 15.08.17.
 */

class MoshiParser(val moshi: com.squareup.moshi.Moshi) : INetworkParser {

    override fun <T> parse(type: Type, json: String?): T? {
        return moshi.adapter<T>(type)
                .nullSafe()
                .fromJson(json)

    }
}

class GsonParser(val gson: Gson) : INetworkParser {

    override fun <T> parse(type: Type, json: String?): T? {
        val adapter = gson.getAdapter(TypeToken.get(type))
        val result = adapter.fromJson(json)
        return result as? T
    }
}