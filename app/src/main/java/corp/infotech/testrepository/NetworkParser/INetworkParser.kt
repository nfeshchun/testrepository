package corp.infotech.testrepository.NetworkParser

import java.lang.reflect.Type

/**
 * Created by nikitafeshchun on 15.08.17.
 */

interface INetworkParser {
    fun <T> parse(type: Type, json: String?): T?
}